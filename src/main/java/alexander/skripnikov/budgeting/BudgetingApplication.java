package alexander.skripnikov.budgeting;

import alexander.skripnikov.budgeting.db.*;
import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.YearMonth;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@SpringBootApplication
@EnableJpaRepositories
@RestController
@Log4j2
public class BudgetingApplication {
  private TransactionDAO transactionDAO;
  private CategoryDAO categoryDAO;
  private StoreDAO storeDAO;

  public static void main(String[] args) {
    SpringApplication.run(BudgetingApplication.class, args);
  }

  @Autowired
  private void setDAOs(TransactionDAO transactionDAO, CategoryDAO categoryDAO, StoreDAO storeDAO) {
    this.transactionDAO = transactionDAO;
    this.categoryDAO = categoryDAO;
    this.storeDAO = storeDAO;
  }

  @PostConstruct
  public void initialize(/*CategoryDAO categoryDAO*/) {
    // Create default categories if not created
    Map<String, List<String>> childrenCategories = Map.of(
      "Food", List.of("Groceries", "Restaurants"),
      "Transportation", List.of("Public transportation", "Private transportation")
    );

    List.of(
      "Housing",
      "Food",
      "Transportation",
      "Healthcare",
      "Personal Care",
      "Entertainment",
      "Clothing",
      "Education",
      "Savings and Investment",
      "Credit Payments",
      "Travel",
      "Gifts and Donation",
      "Pets",
      "Technology",
      "Furniture and home appliance",
      "Other"
    ).forEach(categoryName -> {
      if (categoryDAO.findByName(categoryName) == null) {
        Category category = Category.builder().name(categoryName).custom(false).build();
        categoryDAO.save(category);
        if (childrenCategories.get(categoryName) != null) {
          childrenCategories.get(categoryName).forEach((String subCategoryName) -> {
            Category subCategory = Category.builder().name(subCategoryName).parent(category).custom(false).build();
            categoryDAO.save(subCategory);
          });
        }
      }
    });
  }

  @GetMapping("/transactions")
  public ResponseEntity<List<Transaction>> getMonthlyTransactions(@RequestParam(required = false) YearMonth yearMonth) {
      if (yearMonth == null) {
          yearMonth = YearMonth.now();
      }

      LocalDateTime startOfMonth = yearMonth.atDay(1).atStartOfDay();
      List<Transaction> monthlyTransactions = transactionDAO.getMonthly(startOfMonth);

      return ResponseEntity.ok(monthlyTransactions);
  }

  @PostMapping("/transaction")
  public ResponseEntity<String> createTransaction(
    @RequestParam String title,
    @RequestParam int amount,
    @RequestParam Currency currency,
    @RequestParam Transaction.Flow flow,
    @RequestBody(required = false) TransactionRequest transactionRequest
  ) {
    try {
      Transaction.TransactionBuilder transactionBuilder = Transaction.builder();
      transactionBuilder.title(title);
      transactionBuilder.amount(amount);
      transactionBuilder.currency(currency);
      transactionBuilder.flow(flow);
      if (transactionRequest != null) {
        transactionBuilder.description(transactionRequest.getDescription());
        transactionBuilder.receiptImage(transactionRequest.getReceiptImage());
        transactionRequest.getStoreId().ifPresent(storeId -> {
          Store foundStore = null;
          try {
            foundStore = storeDAO.findById(storeId)
                .orElseThrow(() -> new Exception("Invalid store ID: " + storeId));
            transactionBuilder.store(foundStore);
          } catch (Exception e) {
            log.error("Problem assigning store with ID " + storeId + "\":\n", e.getMessage());
          }
        });

        transactionRequest.getCategoryName().ifPresent(categoryName -> {
          Category foundCategory;
          try {
            foundCategory = categoryDAO.findByName(categoryName);
            transactionBuilder.category(foundCategory);
          } catch (Exception e) {
            log.error("Problem assigning category \"" + categoryName + "\":\n", e.getMessage());
          }
        });

        transactionBuilder.transactionDate(transactionRequest.getTransactionDate());
        transactionBuilder.amount(transactionRequest.getAmount());
        transactionBuilder.currency(transactionRequest.getCurrency());
      }
      transactionDAO.save(transactionBuilder.build());
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error while making a purchase", e);
      return new ResponseEntity<>("Something went wrong while making a purchase:\n\t" + e.getMessage(),
        HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/purchase")
  public ResponseEntity<String> createPurchase(
    @RequestParam String title,
    @RequestParam int amount,
    @RequestParam Currency currency,
    @RequestBody(required = false) TransactionRequest transactionRequest
  ) {
    return createTransaction(title, amount, currency, Transaction.Flow.OUT, transactionRequest);
  }

  @PostMapping("/income")
  public ResponseEntity<String> createIncome(
    @RequestParam String title,
    @RequestParam int amount,
    @RequestParam Currency currency,
    @RequestBody(required = false) TransactionRequest transactionRequest) {
    return createTransaction(title, amount, currency, Transaction.Flow.IN, transactionRequest);
  }

  @PostMapping("/store")
  public ResponseEntity<String> createStore(
    @RequestParam String name,
    @RequestBody(required = false) StoreRequest storeRequest
  ) {
    Store.StoreBuilder storeBuilder = Store.builder();
    storeBuilder.name(name);
    if (storeRequest != null) {
      storeBuilder.description(storeRequest.getDescription());
      storeRequest.getCategoryName().ifPresent(categoryName -> {
        Category foundCategory;
        try {
          foundCategory = categoryDAO.findByName(categoryName);
          storeBuilder.category(foundCategory);
        } catch (Exception e) {
          log.error("Problem assigning category \"" + categoryName + "\":\n", e.getMessage());
        }
      });
    }
    storeDAO.save(storeBuilder.build());
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping("/category")
  public ResponseEntity<String> createCategory(
    @RequestParam String name,
    @RequestParam(required = false) String parentName,
    @RequestParam(required = false, defaultValue = "false") boolean modify
  ) {
    try {
      Category category = categoryDAO.findByName(name);
      if (category != null) {
        if (!modify) {
          throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Category \"" + name +
          "\", you need to specify if you want to modify it with parameter \"modify = true\"");
        }
      } else {
        Category.CategoryBuilder categoryBuilder = Category.builder();
        category = categoryBuilder.name(name).build();
        categoryDAO.save(category);
      }

      if (parentName != null) {
        Category parent = categoryDAO.findByName(parentName);
        if (parent == null) {
          log.error("There's no category with name \"{}\", category \"{}\" will be created without a parent!",
            parentName, name);
        }
        category.setParent(parent);
      }
    } catch (ResponseStatusException e) {
      log.error("Problem creating or modifying category \"{}\":\n", name, e.getReason());
      return new ResponseEntity<>(e.getReason(), e.getStatusCode());
    } catch (Exception e) {
      log.error("Unexpected error when creating or modifying category \"{}\":\n", name, e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
