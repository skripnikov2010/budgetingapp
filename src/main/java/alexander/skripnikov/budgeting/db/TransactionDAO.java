package alexander.skripnikov.budgeting.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface TransactionDAO extends JpaRepository<Transaction, Long> {
  @Query("select e from Transaction e " +
    "where EXTRACT(YEAR FROM :date) || '-' || EXTRACT(MONTH FROM :date) = " +
    "EXTRACT(YEAR FROM e.transactionDate) || '-' || EXTRACT(MONTH FROM e.transactionDate) " +
    "order by e.transactionDate")
  List<Transaction> getMonthly(@Param("date") LocalDateTime date);
}
