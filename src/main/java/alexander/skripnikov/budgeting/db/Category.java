package alexander.skripnikov.budgeting.db;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Category {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  @Column
  private String name;
  @ManyToOne
  @JoinColumn(name="parent_id", referencedColumnName = "id")
  private Category parent;
  @OneToMany(mappedBy = "id.category", cascade = CascadeType.ALL)
  private List<Category> subCategories = new ArrayList<>();
  @Builder.Default
  private boolean custom = true;
}
