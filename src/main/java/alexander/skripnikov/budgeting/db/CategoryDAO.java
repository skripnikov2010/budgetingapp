package alexander.skripnikov.budgeting.db;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryDAO extends JpaRepository<Category, String> {
  Category findByName(String name);
}
