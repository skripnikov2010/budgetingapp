package alexander.skripnikov.budgeting.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StoreRequest {
  private String description;
  private String categoryName;

  public Optional<String> getCategoryName() {
    return Optional.of(categoryName);
  }
}
