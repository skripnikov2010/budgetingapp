package alexander.skripnikov.budgeting.db;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BudgetDAO extends JpaRepository<Budget, Long> {
}
