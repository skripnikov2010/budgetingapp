package alexander.skripnikov.budgeting.db;


import jakarta.persistence.*;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Currency;
import java.util.Map;
import java.util.Optional;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Log4j2
public class Transaction {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  @Column(nullable = false)
  private String title;
  private String description;
  private String receiptImage; //TODO implement image storing in a cloud
  @ManyToOne
  @JoinColumn(name = "store_id", referencedColumnName = "id")
  private Store store;
  @ManyToOne
  @JoinColumn(name = "category_name", referencedColumnName = "name", nullable = false)
  private Category category;
  @Column(nullable = false)
  @Builder.Default
  private LocalDate transactionDate = LocalDate.now();
  @Column(nullable = false)
  private int amount;
  @Column(nullable = false)
  @Builder.Default
  private Currency currency = Currency.getInstance("USD");
  public enum Flow {
   IN, OUT
  }
  @Column(nullable = false)
  private Flow flow = Flow.OUT;
}