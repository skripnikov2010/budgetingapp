package alexander.skripnikov.budgeting.db;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Store {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  @Column(unique = true)
  private String name;
  private String description;
  @ManyToOne
  @JoinColumn(name="category_id", referencedColumnName = "id")
  private Category category; //Will be used if no category is specified for purchases in this store
}
