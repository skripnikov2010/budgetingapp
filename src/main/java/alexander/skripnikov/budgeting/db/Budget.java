package alexander.skripnikov.budgeting.db;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Budget {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  private double startingBalance;
  private double balance;
  private LocalDateTime startTime;
  private LocalDateTime endTime;
  @ManyToOne
  private Category category;

}
