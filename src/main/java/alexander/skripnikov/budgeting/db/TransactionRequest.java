package alexander.skripnikov.budgeting.db;

import lombok.*;

import java.time.LocalDate;
import java.util.Currency;
import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionRequest {
  private String description;
  private String receiptImage;
  private long storeId;
  private String categoryName;
  private LocalDate transactionDate = LocalDate.now();
  private int amount;
  private Currency currency = Currency.getInstance("USD");

  public Optional<Long> getStoreId() {
    return Optional.of(storeId);
  }

  public Optional<String> getCategoryName() {
    return Optional.of(categoryName);
  }
}
