package alexander.skripnikov.budgeting.db;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreDAO extends JpaRepository<Store, Long> {
  
}
